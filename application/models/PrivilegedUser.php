<?php
//require_once "User.php";
class PrivilegedUser extends CI_Model
{
	private $roles;
	public function __construct()
	{
		parent::__construct();
		$this->roles= array();
	}

	//override  User method
	public function getByUsername($username){
		$sql = "SELECT * FROM users WHERE username = '$username'";
		$STH = $this->DBH->query($sql);
		$STH->setFetchMode(PDO::FETCH_OBJ);
		$result = json_decode(json_encode($STH->fetchAll()),true);

		if (!empty($result)) {
			$privUser = new PrivilegedUser();

			$privUser->user_id = $result[0]["user_id"];
			$privUser->username = $username;
			$privUser->password = $result[0]["password"];
			$privUser->email_addr = $result[0]["email_addr"];
			$privUser->initRoles();
			return $privUser;

		}else{
			return false;
		}
	}

	//populate roles with their associated permissions
	protected function initRoles(){
		$this->roles = array();
		$sql = "SELECT t1.role_id, t2.role_name FROM user_role as t1 JOIN roles as t2 ON t1.role_id = t2.role_id WHERE t1.user_id = '$this->user_id'";
		$STH = $this->DBH->query($sql);
		//$sth->execute(array(":user_id"=> $this->user_id));

		while ($row = $STH->fetch(PDO::FETCH_ASSOC)) {
			$this->roles[$row["role_name"]] = Role::getRolePerms($row["role_id"]);
		}
	}

	public function hasPrivilege($perm){
		foreach ($this->roles as $role) {
			if ($role->hasperm ($perm)){
				return true;
			}
		}
		return false;
	}

	public function hasRole($role_name){
		return isset($this->roles[$role_name]);
	}

	public static function insertPerm($role_id, $perm_id){
		$sql = "INSERT INTO role_perm (role_id, perm_id) VALUES (:role_id,:perm_id)";
		$sth = $GLOBALS["DB"]->prepare($sql);
		$sth->execute(array(":role_id"=> $role_id, ":perm_id"=>$perm_id));
	}

	public static function deleteperms(){
		$sql = "TRUNCATE role_perm";
		$sth = $GLOBALS["DB"]->prepare($sql);
	return $sth->execute();
	}
}