<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="#">
    <title>::EIMS::</title>
    <!-- required for search, block3 of 5 start -->
    <link rel="stylesheet" href="<?php echo base_url();?>assets/bootstrap/css/jquery-ui.css">
    <script src="<?php echo base_url();?>assets/bootstrap/js/jquery.js"></script>
    <script src="<?php echo base_url();?>assets/bootstrap/js/jquery-ui.js"></script>


    <link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
    <!--   required for search, block3 of 5 end -->
    <!-- Search in Drop down -->
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.12.1/css/bootstrap-select.css" />
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">


    <link href="<?php echo base_url();?>assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
    <!-- Bootstrap core CSS -->
    <link href="<?php echo base_url();?>assets/css/style.css" rel="stylesheet" type="text/css">

    <!-- Bootstrap Dropdown Hover CSS -->
    <link href="<?php echo base_url();?>assetse/css/animate.min.css" rel="stylesheet">
    <link href="<?php echo base_url();?>assets/css/bootstrap-dropdownhover.min.css" rel="stylesheet">
    <!-- Include Date Range Picker -->
    <script type="text/javascript" src="<?php echo base_url();?>assets/bootstrap/js/daterangepicker.js"></script>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>assets/bootstrap/css/daterangepicker.css" />

    <!-- Required for Datepicker ended -->
    <script src="<?php echo base_url();?>assets/js/index.js"></script>
    <!-- Select Search -->
    <link href='<?php echo base_url();?>assets/select2/dist/css/select2.min.css' rel='stylesheet' type='text/css'>

    <!-- Select Search ended
    <link href='../resource/clock/css/style.css' rel='stylesheet' type='text/css' media="screen">
    -->
</head>
