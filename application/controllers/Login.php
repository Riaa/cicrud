<?php
/**
 * 
 */
class Login extends CI_controller
{
	
	function __construct()
	{
		parent:: __construct();
		$this->load->model('Login_model');
			}


 public function index(){
       
        $this->load->view('includes/header');
        $this->load->view('User/profile/signup');
        $this->load->view('includes/footer_script');
        $this->load->view('includes/footer');
    }

    public function auth(){

    	$email	= $this->input->post('email',TRUE);
    	$password	= md5($this->input->post('password',TRUE));
    	$validate	= $this->Login_model->validate($email,$password);
    	if($validate->num_rows() > 0){
    		$data	= $validate->row_array();
    		$name	=$data['user_name'];
    		$email	=$data['user_email'];
    		$level	=$data['user_level'];
            $roleid  =$data['role'];
            $adminid  =$data['user_id'];
    		$sesdata = array(
    			'username'	=>$name,
    			'email'	=>$email,
    			'level'	=>$level,
                'role' =>$role,
                'adminid' =>$adminid,
    			'logged_in'	=>TRUE
    		);

    		$this->session->set_userdata($sesdata);

    		if($roleid =='1'){
    			redirect('crud');

    		}elseif ($roleid=='2') {
    			redirect('page/staff');
    		}
            else{
                redirect('page/author');
            }
    	}else  {
    		echo $this->session->set_flashdata('msg','Username Or password is Wrong');
    		redirect('login');
    		}

    	
    }
     public function logout(){
    		$this->session->sess_destroy();
    		redirect('login');
    	}
   } 
?>