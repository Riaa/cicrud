<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Crud extends CI_Controller {
 
    public function __construct() {
        //load database in autoload libraries
        parent::__construct();
         if($this->session->userdata('logged_in') !== TRUE){
            redirect('login');
        } 

        $this->load->model('Crudmodel');
        $this->load->model('Role');
        $this->load->model('PrivilegedUser');
        $this->load->helper('url_helper');
        $this->load->helper('form');
        $this->load->library('form_validation');
    }
    public function index(){
        $data['data']=$this->Crudmodel->get_products_list();
        $this->load->view('includes/header');
        $this->load->view('includes/nav');
        $this->load->view('crud/list',$data);
        $this->load->view('includes/footer_script');
        $this->load->view('includes/footer');
    }

    public function create(){

        if (!$this->Role->hasperm('addtransection')) 
           echo "Access  denied";
        

        $this->load->view('includes/header');
        $this->load->view('includes/nav');
        $this->load->view('crud/create');
        $this->load->view('includes/footer_script');
        $this->load->view('includes/footer');
    }

    public function store(){
        $this->Crudmodel->insert_product();
        redirect(base_url());
    }
      public function edit($id){
    $product = $this->db->get_where('products', array('id' => $id))->row();
         $this->load->view('includes/header');
        $this->load->view('includes/nav');
        $this->load->view('crud/edit',array('product'=>$product));
        $this->load->view('includes/footer_script');
        $this->load->view('includes/footer');
      }

       public function update($id){
        $data['data']=$this->Crudmodel->update_product($id);
        redirect(base_url());
    }
    
    public function delete($id){
        $this->db->query("Delete from Products where id=$id");
        redirect(base_url());
    }


}